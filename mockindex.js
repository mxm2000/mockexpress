let express = require('express');   //引入express
let Mock = require('mockjs');       //引入mock
let bodyParser  = require('body-parser')
let ex = express();        //实例化express
const login = require('./json/login.js')
// let login= 'json/login.js'
// console.log(login)

ex.use(bodyParser.urlencoded({extended: false}));
ex.use(bodyParser.json());// 添加json解析
/*Mock.mock('@datetime')*/


ex.all('*',function(req, res, next) {//允许跨域
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

let url=['/model1/model1','/model2/model2','/model3/model3'],port=8007
ex.use(url[0],(req, res)=>{
    // console.log('express监听端口req', req)
    res.json(Mock.mock({
        'status': 200,
        'data|35':[{
            'studentId|1-100': 2,
            'name|1':['张伟', '王二'],
            'remaks|1': ['哈你好', '有你就好', '你喜欢啦期盼', '水在中不在不在', '我希望的有些要求'],
            'type|1': ['收支', '退货', '下单', '发货'],
            'state|0-1': 1,
            'img':'https://element.eleme.cn/static/theme-index-red.c8e136e.png',
            'dataTime':Mock.mock('@now')
        }]
    }))
})

ex.use(url[1],(req, res)=>{
    console.log('express监听端口req', req)
    res.json(Mock.mock({
        'status': 200,
        'data|1-10':[{
            'key|+1': 1,
            'name|1':['字段名2'],
            'remaks|1': ['哈你好', '有你就好', '你喜欢啦期盼', '水在中不在不在', '我希望的有些要求'],
            'type|1': ['收支', '退货', '下单', '发货'],
            'total|1': [20, 60, 55, 910],
        }]
    }))
})

ex.use(url[2],(req, res)=>{
    // console.log('express监听端口 login', res)
    res.json(Mock.mock({
        "msg": "成功！",
        "code": 0,
        "data": {
            "jwtToken": "Bearer xxxyyyzzz",
            "user": {
                "id": "6892",
                "nickName": "有缘",
                "logo": "",
                
                "icons":'person-filled',
                "auType":0,
                "avatar": "@image('200x200','red','#fff','img')", 
                "tag|9":[
                    {'name|1':['单身1','小奶狗3','可盐可甜33','诚信小白','可爱','绿豆眼','长发妹','有米'], 'type|1': ['primary', 'default','primary','success','warning','error','royal']}
                ],
                'type|1': ['primary', 'default','primary','success','warning','error','royal'],
                
            }
        }
    }))
})

module.exports = ex.listen(port, () => {
    url.map(m=>{
       console.log('监听端口','localhost:'+port+m) 
    })
    
})