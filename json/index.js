var Mock = require('mockjs')
let lineCar = require('./lineCar')
let lineCar2 = require('./lineCar2')
let login = require('./login')
module.exports = () => {
  // 使用 Mock
  var data = Mock.mock({
    login
  });
  // 返回的data会作为json-server的数据
  return data;
};